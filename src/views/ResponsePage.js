import { Component } from "react";
import responseJson from "../responseData.json";
import ModalComponent from "./ModalComponent";
import React from "react";
class ResponsePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: responseJson,
        }
        this.addResourse = this.addResourse.bind(this);
    }

    handleAction(e) {
        console.log.apply(e.target);
    }

    addResourse(updatedRes) {
        this.setState(({data}) => ({data: { ...data, updatedRes}}));
    }

    deleteProduct(category, brand, product_id) {
        let products = brand.products.filter(product => product.id !== product_id);
        brand.products = products;
        category.brands = category.brands.map((brnd) => {
            if(brnd.id === brand.id) {
                return brand;
            }
            return brnd;
        });

        let resourse = this.state.data
        var categories = resourse.categories.map((cat) => {
            if(cat.id === category.id) {
                return category;
            }
            return cat;
        });

        resourse.categories = categories;

        this.setState(({data}) => ({data: { ...data, resourse}}));
    }

    render() {
        
      return (
          <div>
              <ModalComponent resourse={this.state.data} addResourse={this.addResourse} />
              {this.state.data.categories.map(category =>
                <div className="categoty" key={category.id}>
                    <div className="c_name_container">
                        <div className="c_name">{category.name}</div>
                        <div className="btn"><button id="delete">Delete</button></div>
                    </div>
                    <div className="c_brands">
                        {category.brands.map(brand => 
                            <div className="brand" key={brand.id}>
                                <div>
                                    <div className="b_name">{brand.name}</div>
                                    <div className="btn"><button id="delete">Delete</button></div>
                                </div>
                                
                                {brand.products.map(product=>
                                    <div key={product.id}>
                                        <div className="product">{product.name}</div>
                                        <div className="btn"><button id="delete" onClick={() => 
                                        this.deleteProduct(category,
                                            brand,
                                            product.id)}>Delete</button></div>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                </div>
                )}

          </div>
      );
    }

    
}
export default ResponsePage;