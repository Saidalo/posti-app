import React, {useState} from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import $ from 'jquery';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

// const CATEGORY = 1;
// const BRAND = 2;
// const PRODUCT = 3;

export default function ModalComponent({resourse, addResourse}) {
  let stateRes = resourse;
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState("category");
  const [category, setCategory] = useState(0);
  const [brands, setBrands] = useState(resourse.categories[category].brands)
  const handleOpen = (a) => {
    setTitle(a);
    setOpen(true);
  }
  const handleClose = () => setOpen(false);

  const addCategory = () => {
    handleOpen('category');
  }

  const addBrand = () => {
    handleOpen('brand');
  }

  const addProduct = () => {
    handleOpen('product');
  }

  const updateBrands = () => {
    var catID = $('#categories').val();
    setBrands(resourse.categories.find(category => category.id === catID).brands);
  }

  function makeid(length, nums = true) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    characters = nums ? (characters + '0123456789') : characters;
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  
  const saveAction = () => {
    if (title.includes('category')) {
      let cat = {
        id: makeid(24),
        name: $("#category_name").val(),
        brands: []
      }
      let categories = stateRes.categories;
      categories.push(cat);
      stateRes.categories = categories;
      addResourse(stateRes);
    }

    if (title.includes('brand')) {
      let cat = stateRes.categories.find(category => category.id === $('#categories').val());
      let brand = {
        id: cat.brands.length > 0 ? (cat.brands[cat.brands.length - 1].id + 1) : 1,
        name: $('#brand_name').val(),
        products: []
      }
      let brands = cat.brands;
      brands.push(brand); 
      cat.brands = brands;

      let resourse = stateRes.categories.map((category) => {
        if (category.id === cat.id) {
          return cat;
        }
        return category;
      });
      addResourse(resourse);
    }

    if (title.includes('product')) {
      let cat = stateRes.categories.find(category => category.id === $('#categories').val());
      let brand = cat.brands.find(brnd => brnd.id === parseInt($('#brands').val()));
      let products = brand.products;
      let product = {
        id: Math.floor(Math.random() * 1000000),
        name: $('#product_name').val()
      }
      products.push(product);
      brand.products = products;
      cat.brands.map((brnd) => {
        if(brnd.id === brand.id) {
          return brand;
        }
        return brand;
      });
      let resourse = stateRes.categories.map((category) => {
        if (category.id === cat.id) {
          return cat;
        }
        return category;
      });
      addResourse(resourse);
    }

    setOpen(false);
  }

  return (
    <div>
      <div className="action-container">
        Actions:
        <div className="btn"><button id="add_categoty" onClick={addCategory}>Add category</button></div>
        <div className="btn"><button id="add_brand" onClick={addBrand}>Add brand</button></div>
        <div className="btn"><button id="add_product" onClick={addProduct}>Add product</button></div>
      </div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add {title}
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <div>
              <div>
                Category
              </div>
              <div>
                {(title !== "category") && <select name="cars" id="categories" className='select-option' onChange={updateBrands}>
                  {resourse.categories.map(category => 
                  <option value={category.id} key={category.id}>{category.name}</option>)}
                </select>}
                {(title === "category") && <input type={"text"} id="category_name" />}
              </div>
              {(title === "brand" || title === "product") && (title !== "category") && 
              <div>
                Brand
              </div>}
              <div>
                {(title !== "brand") && (title !== "category") &&
                <select name="cars" id="brands" className='select-option'>
                  {brands.map(brand => 
                  <option value={brand.id} key={brand.id}>{brand.name}</option>)}
                </select>
                }
                {(title === "brand") && (title !== "category") && <input type={"text"} id="brand_name" />}
              </div>
              {(title !== "brand") && (title !== "category") && <div>
                Product name
              </div>}
              {(title !== "brand") && (title !== "category") && <div>
                <input type={'text'} name="product_name" id="product_name" />
              </div>}
              

              <div className='btn-container'>
                <div className="btn"><button id="save" onClick={saveAction}>Save {title}</button></div>
                <div className="btn"><button id="close" onClick={() => setOpen(false)}>close</button></div>
              </div>
            </div>
          </Typography>
        </Box>
      </Modal>
    </div>
  );
}
